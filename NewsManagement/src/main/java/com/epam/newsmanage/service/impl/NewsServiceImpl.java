package com.epam.newsmanage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanage.dao.NewsDAO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.INewsService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Service
public class NewsServiceImpl implements INewsService {

	@Autowired
	private NewsDAO newsDAO;

	@Override
	public Long createNews(NewsTO news) throws ServiceException {
		Long id = null;
		try {
			id = newsDAO.create(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return id;
	}

	@Override
	public NewsTO readNews(Long id) throws ServiceException {
		NewsTO news = null;
		try {
			news = newsDAO.read(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	@Override
	public void updateNews(NewsTO news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteNews(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<NewsTO> searchNewsByTags(Long[] tagIds) throws ServiceException {
		List<NewsTO> news = new ArrayList<>();
		try {
			news = newsDAO.readNewsByTags(tagIds);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	@Override
	public List<NewsTO> searchNewsByAuthor(Long authorId)
			throws ServiceException {
		List<NewsTO> news = new ArrayList<>();
		try {
			news = newsDAO.readNewsByAuthor(authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	@Override
	public void addNewsTags(Long newsId, List<Long> tagIds)
			throws ServiceException {
		try {
			newsDAO.addNewsTags(newsId, tagIds);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		try {
			newsDAO.addNewsAuthor(newsId, authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<NewsTO> getAllNews() throws ServiceException {
		List<NewsTO> news = new ArrayList<>();
		try {
			news = newsDAO.readAllNews();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	@Override
	public List<NewsTO> getMostCommentedNews() throws ServiceException {
		List<NewsTO> news = new ArrayList<>();
		try {
			news = newsDAO.readMostCommentedNews();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	@Override
	public int getNewsCount() throws ServiceException {
		int count = 0;
		try {
			count = newsDAO.countAllNews();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return count;
	}
}
