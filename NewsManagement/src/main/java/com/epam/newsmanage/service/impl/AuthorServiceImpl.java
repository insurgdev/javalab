package com.epam.newsmanage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanage.dao.AuthorDAO;
import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.IAuthorService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Service
public class AuthorServiceImpl implements IAuthorService {

	@Autowired
	private AuthorDAO authorDAO;

	@Override
	public Long createAuthor(AuthorTO author) throws ServiceException {
		Long id = null;
		try {
			id = authorDAO.create(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return id;
	}

	@Override
	public AuthorTO readAuthor(Long id) throws ServiceException {
		AuthorTO author = null;
		try {
			author = authorDAO.read(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return author;
	}

	@Override
	public void updateAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteAuthor(Long id) throws ServiceException {
		try {
			authorDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		AuthorTO author = null;
		try {
			author = authorDAO.readAuthorByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return author;
	}

	@Override
	public void setAuthorExpired(AuthorTO author) throws ServiceException {
		try {
			authorDAO.setExpired(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
