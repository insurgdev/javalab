package com.epam.newsmanage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanage.dao.CommentDAO;
import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.ICommentService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Service
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private CommentDAO commentDAO;

	@Override
	public Long createComment(CommentTO comment) throws ServiceException {
		Long id = null;
		try {
			id = commentDAO.create(comment);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return id;
	}

	@Override
	public CommentTO readComment(Long id) throws ServiceException {
		CommentTO comment = null;
		try {
			comment = commentDAO.read(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return comment;
	}

	@Override
	public void updateComment(CommentTO comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteComment(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId)
			throws ServiceException {
		List<CommentTO> comments = new ArrayList<>();
		try {
			comments = commentDAO.readCommentsByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return comments;
	}
}
