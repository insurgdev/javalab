package com.epam.newsmanage.service;

import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.exception.ServiceException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
public interface IAuthorService {

	Long createAuthor(AuthorTO author) throws ServiceException;

	AuthorTO readAuthor(Long authorId) throws ServiceException;

	void updateAuthor(AuthorTO author) throws ServiceException;

	void deleteAuthor(Long authorId) throws ServiceException;

	AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException;

	void setAuthorExpired(AuthorTO author) throws ServiceException;
}
