package com.epam.newsmanage.service;

import java.util.List;

import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.ServiceException;

public interface ITagService {

	Long createTag(TagTO tag) throws ServiceException;

	TagTO readTag(Long tagId) throws ServiceException;

	void updateTag(TagTO tag) throws ServiceException;

	void deleteTag(Long tagId) throws ServiceException;

	List<TagTO> getTagsByNewsId(Long newsId) throws ServiceException;
}
