package com.epam.newsmanage.service;

import java.util.List;

import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.ServiceException;

public interface INewsService {

	Long createNews(NewsTO news) throws ServiceException;

	NewsTO readNews(Long newsId) throws ServiceException;

	void updateNews(NewsTO news) throws ServiceException;

	void deleteNews(Long newsId) throws ServiceException;

	List<NewsTO> searchNewsByTags(Long[] tagIds) throws ServiceException;

	List<NewsTO> searchNewsByAuthor(Long authorId) throws ServiceException;

	void addNewsTags(Long newsId, List<Long> tagIds) throws ServiceException;

	void addNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	List<NewsTO> getAllNews() throws ServiceException;

	List<NewsTO> getMostCommentedNews() throws ServiceException;

	int getNewsCount() throws ServiceException;
}
