package com.epam.newsmanage.service;

import java.util.List;

import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.exception.ServiceException;

public interface ICommentService {

	Long createComment(CommentTO comment) throws ServiceException;

	CommentTO readComment(Long commentId) throws ServiceException;

	void updateComment(CommentTO comment) throws ServiceException;

	void deleteComment(Long commentId) throws ServiceException;

	List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException;
}
