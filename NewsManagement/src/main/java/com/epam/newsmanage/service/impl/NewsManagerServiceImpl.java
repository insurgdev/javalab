package com.epam.newsmanage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.entity.NewsVO;
import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.IAuthorService;
import com.epam.newsmanage.service.ICommentService;
import com.epam.newsmanage.service.INewsManagerService;
import com.epam.newsmanage.service.INewsService;
import com.epam.newsmanage.service.ITagService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Service
public class NewsManagerServiceImpl implements INewsManagerService {

	@Autowired
	INewsService newsService;
	@Autowired
	IAuthorService authorService;
	@Autowired
	ITagService tagService;
	@Autowired
	ICommentService commentService;

	@Override
	public void createNewsVO(NewsVO completeNews) throws ServiceException {
		NewsTO news = completeNews.getNews();
		AuthorTO author = completeNews.getAuthor();
		List<TagTO> tags = completeNews.getTags();
		newsService.createNews(news);
		authorService.createAuthor(author);
		List<Long> tagIds = new ArrayList<>();
		for (TagTO tag : tags) {
			Long id = tagService.createTag(tag);
			tagIds.add(id);
		}
		newsService.addNewsAuthor(news.getId(), author.getId());
		newsService.addNewsTags(news.getId(), tagIds);
		for (CommentTO comment : completeNews.getComments()) {
			commentService.createComment(comment);
		}
	}

	@Override
	public NewsVO readNewsVO(Long newsId) throws ServiceException {
		NewsVO completeNews = new NewsVO();
		completeNews.setNews(newsService.readNews(newsId));
		completeNews.setAuthor(authorService.getAuthorByNewsId(newsId));
		completeNews.setTags(tagService.getTagsByNewsId(newsId));
		completeNews.setComments(commentService.getCommentsByNewsId(newsId));
		return completeNews;
	}

	@Override
	public void deleteNewsVO(NewsVO completeNews) throws ServiceException {
		NewsTO news = completeNews.getNews();
		newsService.deleteNews(news.getId());
		authorService.setAuthorExpired(completeNews.getAuthor());
		for (TagTO tag : completeNews.getTags()) {
			tagService.deleteTag(tag.getId());
		}
		for (CommentTO comment : completeNews.getComments()) {
			commentService.deleteComment(comment.getId());
		}
	}
}
