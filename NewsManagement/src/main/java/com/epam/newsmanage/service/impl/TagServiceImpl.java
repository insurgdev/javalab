package com.epam.newsmanage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanage.dao.TagDAO;
import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.ITagService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Service
public class TagServiceImpl implements ITagService {

	@Autowired
	private TagDAO tagDAO;

	@Override
	public Long createTag(TagTO tag) throws ServiceException {
		Long id = null;
		try {
			id = tagDAO.create(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return id;
	}

	@Override
	public TagTO readTag(Long id) throws ServiceException {
		TagTO tag = null;
		try {
			tag = tagDAO.read(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return tag;
	}

	@Override
	public void updateTag(TagTO tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteTag(Long id) throws ServiceException {
		try {
			tagDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws ServiceException {
		List<TagTO> tags = new ArrayList<>();
		try {
			tags = tagDAO.readTagsByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
		return tags;
	}
}
