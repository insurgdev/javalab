package com.epam.newsmanage.service;

import com.epam.newsmanage.entity.NewsVO;
import com.epam.newsmanage.exception.ServiceException;

public interface INewsManagerService {

	void createNewsVO(NewsVO news) throws ServiceException;

	NewsVO readNewsVO(Long newsId) throws ServiceException;

	void deleteNewsVO(NewsVO news) throws ServiceException;
}
