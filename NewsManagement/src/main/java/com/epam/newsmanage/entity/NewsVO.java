package com.epam.newsmanage.entity;

import java.util.List;

public class NewsVO {

	private NewsTO news;
	private AuthorTO author;
	private List<CommentTO> comments;
	private List<TagTO> tags;

	public NewsVO() {
	}

	public NewsTO getNews() {
		return news;
	}

	public void setNews(NewsTO news) {
		this.news = news;
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	public List<TagTO> getTags() {
		return tags;
	}

	public void setTags(List<TagTO> tags) {
		this.tags = tags;
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}
}
