package com.epam.newsmanage.exception;

public class DAOException extends Exception {

	private static final long serialVersionUID = 3277046148541650628L;

	public DAOException() {
		super();
	}

	public DAOException(String message) {
		super(message);
	}
	
	public DAOException(Throwable cause) {
		super(cause);
	}

	public DAOException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
