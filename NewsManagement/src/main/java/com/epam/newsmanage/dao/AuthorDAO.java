package com.epam.newsmanage.dao;

import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.exception.DAOException;

public interface AuthorDAO extends AbstractDAO<AuthorTO, Long> {

	AuthorTO readAuthorByNews(Long newsID) throws DAOException;

	void setExpired(AuthorTO author) throws DAOException;
}
