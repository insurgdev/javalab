package com.epam.newsmanage.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanage.dao.NewsDAO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.utility.DAOUtility;

@Repository
public class NewsDAOImpl implements NewsDAO {

	private final static String SQL_INSERT_NEWS = "BEGIN INSERT INTO News(news_id,"
			+ " title, short_text, full_text, creation_date, modification_date)"
			+ " VALUES (news_seq.nextval, ?, ?, ?, ?, ?) RETURNING news_id INTO ?; END;";
	private final static String SQL_READ_NEWS = "SELECT news_id, title, short_text, "
			+ "full_text, creation_date, modification_date FROM News WHERE news_id = ?";
	private final static String SQL_UPDATE_NEWS = "UPDATE News SET title = ?,"
			+ " short_text = ?, full_text = ?, creation_date = ?,"
			+ " modification_date = ? WHERE news_id = ?";
	private final static String SQL_DELETE_NEWS = "DELETE FROM News WHERE news_id = ?";
	private final static String SQL_READ_NEWS_BY_AUTHOR = "SELECT N.news_id, "
			+ "N.title, N.short_text, N.full_text, N.creation_date, "
			+ "N.modification_date FROM News N INNER JOIN News_Author NA ON "
			+ "N.news_id = NA.news_id WHERE NA.author_id = ?";
	private final static String SQL_READ_NEWS_BY_TAGS = "SELECT DISTINCT N.news_id, "
			+ "N.title, N.short_text, N.full_text, N.creation_date, "
			+ "N.modification_date FROM News N INNER JOIN News_Tag NT ON "
			+ "N.news_id = NT.news_id WHERE NT.tag_id IN(?)";
	private final static String SQL_ADD_NEWS_TAGS = "INSERT INTO News_Tag"
			+ "(news_id, tag_id) VALUE (?, ?)";
	private final static String SQL_ADD_NEWS_AUTHOR = "INSERT INTO News_Author"
			+ "(news_id, author_id) VALUES(?, ?)";
	private final static String SQL_READ_ALL_NEWS = "SELECT news_id, title, "
			+ "short_text, full_text, creation_date, modification_date FROM News";
	private final static String SQL_READ_MOST_COMMENTED_NEWS = "SELECT N.news_id,"
			+ " N.title, N.short_text, N.full_text, N.creation_date,"
			+ " N.modification_date FROM News N INNER JOIN (SELECT news_id,"
			+ " COUNT(*) as comments_count FROM Comments GROUP BY news_id"
			+ " ORDER BY comments_count) C ON N.news_id = C.news_id";
	private final static String SQL_COUNT_ALL_NEWS = "SELECT COUNT(*) as "
			+ "news_count FROM News";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long create(NewsTO news) throws DAOException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		Long id = null;
		try {
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall(SQL_INSERT_NEWS);
			callableStatement.setString(1, news.getTitle());
			callableStatement.setString(2, news.getShortText());
			callableStatement.setString(3, news.getFullText());
			callableStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			callableStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			callableStatement.registerOutParameter(6, Types.NUMERIC);
			callableStatement.execute();
			id = callableStatement.getLong(6);
			news.setId(id);
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, callableStatement);
		}
		return id;
	}

	@Override
	public NewsTO read(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		NewsTO news = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news = createEntity(resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return news;
	}

	@Override
	public void update(NewsTO news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			preparedStatement.setLong(6, news.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<NewsTO> readNewsByAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> news = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_NEWS_BY_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return news;
	}

	@Override
	public List<NewsTO> readNewsByTags(Long[] tagIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> news = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_NEWS_BY_TAGS);
			preparedStatement.setArray(1,
					connection.createArrayOf("number", tagIds));
			resultSet = preparedStatement.executeQuery();
			news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return news;
	}

	@Override
	public void addNewsTags(Long newsId, List<Long> tagIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAGS);
			preparedStatement.setLong(1, newsId);
			for (Long tagId : tagIds) {
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void addNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<NewsTO> readAllNews() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> news = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_ALL_NEWS);
			resultSet = preparedStatement.executeQuery();
			news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return news;
	}

	@Override
	public List<NewsTO> readMostCommentedNews() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> news = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_MOST_COMMENTED_NEWS);
			resultSet = preparedStatement.executeQuery();
			news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return news;
	}

	@Override
	public int countAllNews() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int count = 0;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_COUNT_ALL_NEWS);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				count = resultSet.getInt("news_count");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return count;
	}

	@Override
	public NewsTO createEntity(ResultSet resultSet) throws SQLException {
		NewsTO news = new NewsTO();
		news.setId(resultSet.getLong("news_id"));
		news.setTitle(resultSet.getString("title"));
		news.setShortText(resultSet.getString("short_text"));
		news.setFullText(resultSet.getString("full_text"));
		news.setCreationDate(resultSet.getTimestamp("creation_date"));
		news.setModificationDate(resultSet.getDate("modification_date"));
		return news;
	}
}
