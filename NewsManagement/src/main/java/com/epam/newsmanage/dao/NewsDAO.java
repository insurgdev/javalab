package com.epam.newsmanage.dao;

import java.util.List;

import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
public interface NewsDAO extends AbstractDAO<NewsTO, Long> {

	List<NewsTO> readNewsByAuthor(Long authorId) throws DAOException;

	List<NewsTO> readNewsByTags(Long[] tagIds) throws DAOException;

	void addNewsTags(Long newsId, List<Long> tagIds) throws DAOException;

	void addNewsAuthor(Long newsId, Long authorId) throws DAOException;

	List<NewsTO> readAllNews() throws DAOException;

	List<NewsTO> readMostCommentedNews() throws DAOException;

	int countAllNews() throws DAOException;
}
