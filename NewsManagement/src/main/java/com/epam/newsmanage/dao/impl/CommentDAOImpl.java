package com.epam.newsmanage.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanage.dao.CommentDAO;
import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.utility.DAOUtility;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Repository
public class CommentDAOImpl implements CommentDAO {

	private final static String SQL_INSERT_COMMENT = "BEGIN INSERT INTO Comments"
			+ "(comment_id, comment_text, creation_date, news_id) VALUES"
			+ "(comments_seq.nextval, ?, ?, ?) RETURNING comment_id INTO ?; END;";
	private final static String SQL_READ_COMMENT = "SELECT comment_id, "
			+ "comment_text, creation_date, news_id FROM Comments WHERE comment_id = ?";
	private final static String SQL_UPDATE_COMMENT = "UPDATE Comments SET "
			+ "comment_text = ?, creation_date = ?, news_id = ? WHERE comment_id = ?";
	private final static String SQL_DELETE_COMMENT = "DELETE FROM Comments WHERE comment_id = ?";
	private static final String SQL_READ_COMMENTS_BY_NEWS = "SELECT comment_id,"
			+ " comment_text, creation_date, news_id FROM Comments WHERE news_id = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long create(CommentTO comment) throws DAOException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		Long id = null;
		try {
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall(SQL_INSERT_COMMENT);
			callableStatement.setString(1, comment.getText());
			callableStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()));
			callableStatement.setLong(3, comment.getNewsId());
			callableStatement.registerOutParameter(4, Types.NUMERIC);
			callableStatement.execute();
			id = callableStatement.getLong(4);
			comment.setId(id);
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, callableStatement);
		}
		return id;
	}

	@Override
	public CommentTO read(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CommentTO comment = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_COMMENT);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				comment = createEntity(resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return comment;
	}

	@Override
	public void update(CommentTO comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setString(1, comment.getText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.setLong(4, comment.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<CommentTO> readCommentsByNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<CommentTO> comments = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_COMMENTS_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			comments = new ArrayList<>();
			while (resultSet.next()) {
				comments.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return comments;
	}

	@Override
	public CommentTO createEntity(ResultSet resultSet) throws SQLException {
		CommentTO comment = new CommentTO();
		comment.setId(resultSet.getLong("comment_id"));
		comment.setText(resultSet.getString("comment_text"));
		comment.setCreationDate(resultSet.getTimestamp("creation_date"));
		comment.setNewsId(resultSet.getLong("news_id"));
		return comment;
	}
}
