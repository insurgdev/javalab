package com.epam.newsmanage.dao;

import java.util.List;

import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.DAOException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
public interface TagDAO extends AbstractDAO<TagTO, Long> {

	List<TagTO> readTagsByNews(Long newsId) throws DAOException;
}
