package com.epam.newsmanage.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanage.dao.AuthorDAO;
import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.utility.DAOUtility;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@Repository("authorDAO")
public class AuthorDAOImpl implements AuthorDAO {

	private final static String SQL_INSERT_AUTHOR = "BEGIN INSERT INTO Author(author_id,"
			+ " author_name, expired) VALUES(author_seq.NEXTVAL, ?, ?)"
			+ " RETURNING author_id INTO ?; END;";
	private final static String SQL_READ_AUTHOR = "SELECT author_id, author_name, "
			+ "expired FROM Author WHERE author_id = ?";
	private final static String SQL_UPDATE_AUTHOR = "UPDATE Author SET author_name"
			+ " = ?, expired = ? WHERE author_id = ?";
	private final static String SQL_DELETE_AUTHOR = "DELETE FROM Author WHERE "
			+ "author_id = ?";
	private final static String SQL_READ_AUTHOR_BY_NEWS = "SELECT  A.author_id, "
			+ "A.author_name, A.expired FROM Author A INNER JOIN News_Author NA ON "
			+ "A.author_id = NA.author_id WHERE NA.news_id = ?";
	private final static String SQL_SET_AUTHOR_EXPIRED = "UPDATE Author SET "
			+ "expired = ? WHERE author_id = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long create(AuthorTO author) throws DAOException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		Long id = null;
		try {
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall(SQL_INSERT_AUTHOR);
			callableStatement.setString(1, author.getName());
			callableStatement.setTimestamp(2, new Timestamp(author.getExpired()
					.getTime()));
			callableStatement.registerOutParameter(3, Types.NUMERIC);
			callableStatement.execute();
			id = callableStatement.getLong(3);
			author.setId(id);
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, callableStatement);
		}
		return id;
	}

	@Override
	public AuthorTO read(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		AuthorTO author = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_AUTHOR);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = createEntity(resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return author;
	}

	@Override
	public void update(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, author.getName());
			preparedStatement.setTimestamp(2, new Timestamp(author.getExpired()
					.getTime()));
			preparedStatement.setLong(3, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public AuthorTO readAuthorByNews(Long newsID) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		AuthorTO author = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_AUTHOR_BY_NEWS);
			preparedStatement.setLong(1, newsID);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = createEntity(resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return author;
	}

	@Override
	public void setExpired(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SET_AUTHOR_EXPIRED);
			preparedStatement.setTimestamp(1, new Timestamp(author.getExpired()
					.getTime()));
			preparedStatement.setLong(2, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public AuthorTO createEntity(ResultSet resultSet) throws SQLException {
		AuthorTO author = new AuthorTO();
		author.setId(resultSet.getLong("author_id"));
		author.setName(resultSet.getString("author_name"));
		author.setExpired(resultSet.getTimestamp("expired"));
		return author;
	}
}
