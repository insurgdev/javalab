package com.epam.newsmanage.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanage.dao.TagDAO;
import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.utility.DAOUtility;

@Repository
public class TagDAOImpl implements TagDAO {

	private final static String SQL_INSERT_TAG = "BEGIN INSERT INTO Tag(tag_id, "
			+ "tag_name) VALUES(tag_seq.nextval, ?) RETURNING tag_id INTO ?; END;";
	private final static String SQL_READ_TAG = "SELECT tag_id, tag_name FROM"
			+ " Tag WHERE tag_id = ?";
	private final static String SQL_UPDATE_TAG = "UPDATE Tag SET tag_name = ?"
			+ " WHERE tag_id = ?";
	private final static String SQL_DELETE_TAG = "DELETE FROM Tag WHERE tag_id = ?";
	private final static String SQL_READ_TAGS_BY_NEWS = "SELECT T.tag_id, "
			+ "T.tag_name FROM Tag T INNER JOIN News_Tag NT ON T.tag_id = "
			+ "NT.tag_id WHERE NT.news_id = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long create(TagTO tag) throws DAOException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		Long id = null;
		try {
			connection = dataSource.getConnection();
			callableStatement = connection.prepareCall(SQL_INSERT_TAG);
			callableStatement.setString(1, tag.getName());
			callableStatement.registerOutParameter(2, Types.NUMERIC);
			callableStatement.executeUpdate();
			id = callableStatement.getLong(2);
			tag.setId(id);
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, callableStatement);
		}
		return id;
	}

	@Override
	public TagTO read(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		TagTO tag = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_TAG);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = createEntity(resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return tag;
	}

	@Override
	public void update(TagTO tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<TagTO> readTagsByNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<TagTO> tags = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READ_TAGS_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			tags = new ArrayList<>();
			while (resultSet.next()) {
				tags.add(createEntity(resultSet));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return tags;
	}

	@Override
	public TagTO createEntity(ResultSet resultSet) throws SQLException {
		TagTO tag = new TagTO();
		tag.setId(resultSet.getLong("tag_id"));
		tag.setName(resultSet.getString("tag_name"));
		return tag;
	}
}
