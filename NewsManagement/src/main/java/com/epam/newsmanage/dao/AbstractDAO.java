package com.epam.newsmanage.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.newsmanage.exception.DAOException;

/**
 * 
 * @author Aliaksandr_Rymarchyk
 *
 * @param <E>
 * @param <Key>
 */
public interface AbstractDAO<E, Key> {

	Key create(E e) throws DAOException;

	E read(Key id) throws DAOException;

	void update(E e) throws DAOException;

	void delete(Key id) throws DAOException;

	E createEntity(ResultSet resultSet) throws SQLException;
}
