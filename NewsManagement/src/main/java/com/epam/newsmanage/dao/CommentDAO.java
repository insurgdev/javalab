package com.epam.newsmanage.dao;

import java.util.List;

import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.exception.DAOException;

public interface CommentDAO extends AbstractDAO<CommentTO, Long> {

	List<CommentTO> readCommentsByNews(Long newsId) throws DAOException;
}
