package com.epam.newsmanage.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanage.dao.CommentDAO;
import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	@Mock
	CommentDAO commentDAO;

	@Autowired
	@InjectMocks
	CommentServiceImpl commentService;

	@Test
	public void createComment() throws ServiceException, DAOException {
		Long commentId = null;
		CommentTO comment = new CommentTO();
		when(commentDAO.create(comment)).thenReturn(comment.getId());
		commentId = commentService.createComment(comment);
		verify(commentDAO).create(comment);
		assertEquals(commentId, comment.getId());
	}

	@Test
	public void readComment() throws ServiceException, DAOException {
		CommentTO comment = new CommentTO();
		when(commentDAO.read(comment.getId())).thenReturn(comment);
		CommentTO nComment = commentService.readComment(comment.getId());
		verify(commentDAO).read(comment.getId());
		assertEquals(comment, nComment);
	}

	@Test
	public void updateComment() throws ServiceException, DAOException {
		CommentTO comment = new CommentTO();
		doNothing().when(commentDAO).update(comment);
		commentService.updateComment(comment);
		verify(commentDAO).update(comment);
	}

	@Test
	public void deleteComment() throws ServiceException, DAOException {
		CommentTO comment = new CommentTO();
		doNothing().when(commentDAO).delete(comment.getId());
		commentService.deleteComment(comment.getId());
		verify(commentDAO).delete(comment.getId());
	}

	@Test
	public void getCommentsByNewsId() throws ServiceException, DAOException {
		List<CommentTO> comments = new ArrayList<>();
		NewsTO news = new NewsTO();
		when(commentDAO.readCommentsByNews(news.getId())).thenReturn(comments);
		List<CommentTO> nComments = commentService.getCommentsByNewsId(news
				.getId());
		verify(commentDAO).readCommentsByNews(news.getId());
		assertEquals(comments, nComments);
	}
}
