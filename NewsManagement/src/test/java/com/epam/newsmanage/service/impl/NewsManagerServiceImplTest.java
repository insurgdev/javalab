package com.epam.newsmanage.service.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.entity.NewsVO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;
import com.epam.newsmanage.service.IAuthorService;
import com.epam.newsmanage.service.ICommentService;
import com.epam.newsmanage.service.INewsService;
import com.epam.newsmanage.service.ITagService;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagerServiceImplTest {

	@Mock
	IAuthorService authorService;
	@Mock
	INewsService newsService;
	@Mock
	ICommentService commentService;
	@Mock
	ITagService tagService;

	@Autowired
	@InjectMocks
	NewsManagerServiceImpl newsManagerService;

	@Test
	public void createNewsVO() throws ServiceException, DAOException {
		NewsTO news = new NewsTO();
		AuthorTO author = new AuthorTO();
		List<Long> tagIds = new ArrayList<>();
		NewsVO newsVO = new NewsVO();

		when(newsService.createNews(newsVO.getNews())).thenReturn(news.getId());
		when(authorService.createAuthor(newsVO.getAuthor())).thenReturn(
				author.getId());
		doNothing().when(newsService).addNewsAuthor(news.getId(),
				author.getId());
		doNothing().when(newsService).addNewsTags(news.getId(), tagIds);

		newsManagerService.createNewsVO(newsVO);

		verify(newsService).createNews(newsVO.getNews());
		verify(authorService).createAuthor(newsVO.getAuthor());
		verify(newsService).addNewsAuthor(newsVO.getNews().getId(),
				newsVO.getAuthor().getId());
		verify(newsService).addNewsTags(newsVO.getNews().getId(), tagIds);

		// InOrder inOrder = inOrder(newsService, authorService);
		// inOrder.verify(newsService).createNews(news);
		// inOrder.verify(authorService).createAuthor(author);
		// inOrder.verify(newsService).addNewsAuthor(news.getId(),
		// author.getId());
		// inOrder.verify(newsService).addNewsTags(news.getId(), tagIds);
	}

	@Test
	public void readNewsVO() throws ServiceException, DAOException {
		Long newsId = null;
		newsManagerService.readNewsVO(newsId);
		verify(newsService).readNews(newsId);
		verify(authorService).getAuthorByNewsId(newsId);
		verify(tagService).getTagsByNewsId(newsId);
		verify(commentService).getCommentsByNewsId(newsId);
	}

	@Test
	public void deleteNewsVO() throws ServiceException, DAOException {
		NewsVO newsVO = new NewsVO();
		newsManagerService.deleteNewsVO(newsVO);
		verify(newsService).deleteNews(newsVO.getNews().getId());
		verify(authorService).setAuthorExpired(newsVO.getAuthor());
	}
}
