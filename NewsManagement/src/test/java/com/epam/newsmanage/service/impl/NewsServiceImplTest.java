/**
 * 
 */
package com.epam.newsmanage.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanage.dao.NewsDAO;
import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	@Mock
	NewsDAO newsDAO;

	@Autowired
	@InjectMocks
	NewsServiceImpl newsService;

	@Test
	public void createNews() throws ServiceException, DAOException {
		Long newsId = null;
		NewsTO news = new NewsTO();
		when(newsDAO.create(news)).thenReturn(news.getId());
		newsId = newsService.createNews(news);
		verify(newsDAO).create(news);
		assertEquals(newsId, news.getId());
	}

	@Test
	public void readNews() throws ServiceException, DAOException {
		NewsTO news = new NewsTO();
		when(newsDAO.read(news.getId())).thenReturn(news);
		NewsTO nNews = newsService.readNews(news.getId());
		verify(newsDAO).read(news.getId());
		assertEquals(news, nNews);
	}

	@Test
	public void updateNews() throws ServiceException, DAOException {
		NewsTO news = new NewsTO();
		doNothing().when(newsDAO).update(news);
		newsService.updateNews(news);
		verify(newsDAO).update(news);
	}

	@Test
	public void deleteNews() throws ServiceException, DAOException {
		NewsTO news = new NewsTO();
		doNothing().when(newsDAO).delete(news.getId());
		newsService.deleteNews(news.getId());
		verify(newsDAO).delete(news.getId());
	}

	@Test
	public void getNewsByTags() throws ServiceException, DAOException {
		List<NewsTO> news = new ArrayList<>();
		Long[] tags = new Long[100];
		when(newsDAO.readNewsByTags(tags)).thenReturn(news);
		List<NewsTO> nNews = newsService.searchNewsByTags(tags);
		verify(newsDAO).readNewsByTags(tags);
		assertEquals(news, nNews);
	}

	@Test
	public void getNewsByAuthor() throws ServiceException, DAOException {
		List<NewsTO> news = new ArrayList<>();
		AuthorTO author = new AuthorTO();
		when(newsDAO.readNewsByAuthor(author.getId())).thenReturn(news);
		List<NewsTO> nNews = newsService.searchNewsByAuthor(author.getId());
		verify(newsDAO).readNewsByAuthor(author.getId());
		assertEquals(news, nNews);
	}

	@Test
	public void addNewsTags() throws ServiceException, DAOException {
		Long newsId = 1L;
		List<Long> tagIds = new ArrayList<>();
		doNothing().when(newsDAO).addNewsTags(newsId, tagIds);
		newsService.addNewsTags(newsId, tagIds);
		verify(newsDAO).addNewsTags(newsId, tagIds);
	}

	@Test
	public void addNewsAuthor() throws ServiceException, DAOException {
		Long newsId = 1L;
		Long authorId = 1L;
		doNothing().when(newsDAO).addNewsAuthor(newsId, authorId);
		newsService.addNewsAuthor(newsId, authorId);
		verify(newsDAO).addNewsAuthor(newsId, authorId);
	}

	@Test
	public void readAllNews() throws ServiceException, DAOException {
		List<NewsTO> news = new ArrayList<>();
		when(newsDAO.readAllNews()).thenReturn(news);
		List<NewsTO> nNews = newsService.getAllNews();
		verify(newsDAO).readAllNews();
		assertEquals(news, nNews);
	}

	@Test
	public void readMostCommentedNews() throws ServiceException, DAOException {
		List<NewsTO> news = new ArrayList<>();
		when(newsDAO.readMostCommentedNews()).thenReturn(news);
		List<NewsTO> nNews = newsService.getMostCommentedNews();
		verify(newsDAO).readMostCommentedNews();
		assertEquals(news, nNews);
	}

	@Test
	public void countAllNews() throws ServiceException, DAOException {
		int count = 3;
		when(newsDAO.countAllNews()).thenReturn(count);
		int nCount = newsService.getNewsCount();
		verify(newsDAO).countAllNews();
		assertEquals(count, nCount);
	}
}
