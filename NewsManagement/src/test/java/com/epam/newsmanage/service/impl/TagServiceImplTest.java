/**
 * 
 */
package com.epam.newsmanage.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanage.dao.TagDAO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;

/**
 * @tag Aliaksandr_Rymarchyk
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	@Mock
	TagDAO tagDAO;

	@Autowired
	@InjectMocks
	TagServiceImpl tagService;

	@Test
	public void createTag() throws ServiceException, DAOException {
		Long tagId = null;
		TagTO tag = new TagTO();
		when(tagDAO.create(tag)).thenReturn(tag.getId());
		tagId = tagService.createTag(tag);
		verify(tagDAO).create(tag);
		assertEquals(tagId, tag.getId());
	}

	@Test
	public void readTag() throws ServiceException, DAOException {
		TagTO tag = new TagTO();
		when(tagDAO.read(tag.getId())).thenReturn(tag);
		TagTO nTag = tagService.readTag(tag.getId());
		verify(tagDAO).read(tag.getId());
		assertEquals(tag, nTag);
	}

	@Test
	public void updateTag() throws ServiceException, DAOException {
		TagTO tag = new TagTO();
		doNothing().when(tagDAO).update(tag);
		tagService.updateTag(tag);
		verify(tagDAO).update(tag);
	}

	@Test
	public void deleteTag() throws ServiceException, DAOException {
		TagTO tag = new TagTO();
		doNothing().when(tagDAO).delete(tag.getId());
		tagService.deleteTag(tag.getId());
		verify(tagDAO).delete(tag.getId());
	}

	@Test
	public void getTagsByNewsId() throws ServiceException, DAOException {
		List<TagTO> tags = new ArrayList<>();
		NewsTO news = new NewsTO();
		when(tagDAO.readTagsByNews(news.getId())).thenReturn(tags);
		List<TagTO> nTags = tagService.getTagsByNewsId(news.getId());
		verify(tagDAO).readTagsByNews(news.getId());
		assertEquals(tags, nTags);
	}
}
