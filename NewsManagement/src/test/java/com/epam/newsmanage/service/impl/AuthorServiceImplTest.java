/**
 * 
 */
package com.epam.newsmanage.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanage.dao.AuthorDAO;
import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.epam.newsmanage.exception.ServiceException;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	/**
	 * authorDAO object, which will be inject into AuthorServiceImpl
	 */
	@Mock
	AuthorDAO authorDAO;

	@Autowired
	@InjectMocks
	AuthorServiceImpl authorService;

	@Test
	public void createAuthor() throws ServiceException, DAOException {
		Long authorId = null;
		AuthorTO author = new AuthorTO();
		when(authorDAO.create(author)).thenReturn(author.getId());
		authorId = authorService.createAuthor(author);
		verify(authorDAO).create(author);
		assertEquals(authorId, author.getId());
	}

	@Test
	public void readAuthor() throws ServiceException, DAOException {
		AuthorTO author = new AuthorTO();
		when(authorDAO.read(author.getId())).thenReturn(author);
		AuthorTO nAuthor = authorService.readAuthor(author.getId());
		verify(authorDAO).read(author.getId());
		assertEquals(author, nAuthor);
	}

	@Test
	public void updateAuthor() throws ServiceException, DAOException {
		AuthorTO author = new AuthorTO();
		doNothing().when(authorDAO).update(author);
		authorService.updateAuthor(author);
		verify(authorDAO).update(author);
	}

	@Test
	public void deleteAuthor() throws ServiceException, DAOException {
		AuthorTO author = new AuthorTO();
		doNothing().when(authorDAO).delete(author.getId());
		authorService.deleteAuthor(author.getId());
		verify(authorDAO).delete(author.getId());
	}

	@Test
	public void getAuthorByNewsId() throws ServiceException, DAOException {
		AuthorTO author = new AuthorTO();
		NewsTO news = new NewsTO();
		when(authorDAO.readAuthorByNews(news.getId())).thenReturn(author);
		AuthorTO nAuthor = authorService.getAuthorByNewsId(news.getId());
		verify(authorDAO).readAuthorByNews(news.getId());
		assertEquals(author, nAuthor);
	}

	@Test
	public void setAuthorExpired() throws ServiceException, DAOException {
		AuthorTO author = new AuthorTO();
		doNothing().when(authorDAO).setExpired(author);
		authorService.setAuthorExpired(author);
		verify(authorDAO).setExpired(author);
	}
}
