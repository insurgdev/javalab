/**
 * 
 */
package com.epam.newsmanage.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanage.dao.CommentDAO;
import com.epam.newsmanage.entity.CommentTO;
import com.epam.newsmanage.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * @Comment Aliaksandr_Rymarchyk
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context-test.xml")
@DatabaseSetup(value = "classpath:comments.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class CommentDAOImplTest {

	@Autowired
	private CommentDAO commentDAO;

	@Test
	public void create() throws DAOException {
		CommentTO comment = new CommentTO();
		comment.setText("helloworld");
		comment.setCreationDate(new Date());
		comment.setNewsId(1L);
		commentDAO.create(comment);
		assertTrue(comment.getId() >= 1);
	}

	@Test
	public void read() throws DAOException {
		Long id = 1L;
		CommentTO comment = commentDAO.read(id);
		assertEquals(comment.getText(), "Good");
	}

	@Test
	public void update() throws DAOException {
		Long id = 2L;
		CommentTO comment = commentDAO.read(id);
		comment.setText("i was wrong, it's good news");
		commentDAO.update(comment);
		CommentTO newComment = commentDAO.read(id);
		assertEquals(comment.getText(), newComment.getText());
	}

	@Test
	public void delete() throws DAOException {
		Long id = 3L;
		commentDAO.delete(id);
		assertEquals(commentDAO.read(id), null);
	}

	@Test
	public void readByNews() throws DAOException {
		Long newsId = 2L;
		List<CommentTO> comments = commentDAO.readCommentsByNews(newsId);
		assertEquals(comments.size(), 2);
	}
}
