/**
 * 
 */
package com.epam.newsmanage.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanage.dao.AuthorDAO;
import com.epam.newsmanage.entity.AuthorTO;
import com.epam.newsmanage.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:authors.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class AuthorDAOImplTest {

	@Autowired
	private AuthorDAO authorDAO;

	@Test
	public void create() throws DAOException {
		AuthorTO author = new AuthorTO();
		author.setName("Alexander");
		author.setExpired(new Date());
		authorDAO.create(author);
		assertTrue(author.getId() >= 1);
	}

	@Test
	public void read() throws DAOException {
		Long id = 1L;
		AuthorTO author = authorDAO.read(id);
		assertEquals(author.getName(), "Alex");
	}

	@Test
	public void update() throws DAOException {
		Long id = 2L;
		AuthorTO author = authorDAO.read(id);
		author.setName("Dima");
		authorDAO.update(author);
		AuthorTO newAuthor = authorDAO.read(id);
		assertEquals(newAuthor.getName(), author.getName());
	}

	@Test
	public void delete() throws DAOException {
		Long id = 3L;
		authorDAO.delete(id);
		assertEquals(authorDAO.read(id), null);
	}

	@Test
	public void readByNews() throws DAOException {
		Long newsId = 2L;
		AuthorTO author = authorDAO.readAuthorByNews(newsId);
		assertEquals(author.getName(), "Slava");
	}

	@Test
	public void setExpired() throws DAOException {
		Long id = 1L;
		AuthorTO author = authorDAO.read(id);
		author.setExpired(new Date());
		authorDAO.setExpired(author);
		AuthorTO newAuthor = authorDAO.read(id);
		assertEquals(author.getExpired(), newAuthor.getExpired());
	}
}
