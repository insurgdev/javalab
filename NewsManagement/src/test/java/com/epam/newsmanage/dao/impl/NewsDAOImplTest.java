/**
 * 
 */
package com.epam.newsmanage.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanage.dao.NewsDAO;
import com.epam.newsmanage.entity.NewsTO;
import com.epam.newsmanage.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context-test.xml")
@DatabaseSetup(value = "classpath:news.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOImplTest {

	@Autowired
	private NewsDAO newsDAO;

	@Test
	public void create() throws DAOException {
		NewsTO news = new NewsTO();
		news.setTitle("title");
		news.setShortText("shortText");
		news.setFullText("fullText");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		newsDAO.create(news);
		assertTrue(news.getId() >= 1);
	}

	@Test
	public void read() throws DAOException {
		Long id = 1L;
		NewsTO news = newsDAO.read(id);
		assertEquals(news.getTitle(), "title1");
	}

	@Test
	public void update() throws DAOException {
		Long id = 2L;
		NewsTO news = newsDAO.read(id);
		news.setTitle("newTitle");
		news.setCreationDate(new Date());
		newsDAO.update(news);
		NewsTO newNews = newsDAO.read(id);
		assertEquals(news.getTitle(), newNews.getTitle());
		assertEquals(news.getCreationDate(), newNews.getCreationDate());
	}

	@Test
	public void delete() throws DAOException {
		Long id = 3L;
		newsDAO.delete(id);
		assertEquals(newsDAO.read(id), null);
	}

	@Test
	public void readByAuthor() throws DAOException {
		Long authorId = 1L;
		List<NewsTO> news = newsDAO.readNewsByAuthor(authorId);
		assertEquals(news.size(), 2);
	}

	@Test
	public void readAll() throws DAOException {
		List<NewsTO> news = newsDAO.readAllNews();
		assertEquals(news.size(), 3);
	}

	@Test
	public void countAll() throws DAOException {
		int count = newsDAO.countAllNews();
		assertEquals(count, 3);
	}
}
