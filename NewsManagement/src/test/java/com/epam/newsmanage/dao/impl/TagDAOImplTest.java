/**
 * 
 */
package com.epam.newsmanage.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanage.dao.TagDAO;
import com.epam.newsmanage.entity.TagTO;
import com.epam.newsmanage.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * @author Aliaksandr_Rymarchyk
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context-test.xml")
@DatabaseSetup(value = "classpath:tags.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOImplTest {

	@Autowired
	private TagDAO tagDAO;

	@Test
	public void create() throws DAOException {
		TagTO tag = new TagTO();
		tag.setName("biathlon");
		tagDAO.create(tag);
		assertTrue(tag.getId() >= 1);
	}

	@Test
	public void read() throws DAOException {
		Long id = 1L;
		TagTO tag = tagDAO.read(id);
		assertEquals(tag.getName(), "football");
	}

	@Test
	public void update() throws DAOException {
		Long id = 2L;
		TagTO tag = tagDAO.read(id);
		tag.setName("gtaIV");
		tagDAO.update(tag);
		TagTO newTag = tagDAO.read(id);
		assertEquals(tag.getName(), newTag.getName());
	}

	@Test
	public void delete() throws DAOException {
		Long id = 3L;
		tagDAO.delete(id);
		assertEquals(tagDAO.read(id), null);
	}

	@Test
	public void readByNews() throws DAOException {
		Long newsId = 2L;
		List<TagTO> tags = tagDAO.readTagsByNews(newsId);
		assertEquals(tags.size(), 3);
	}
}
