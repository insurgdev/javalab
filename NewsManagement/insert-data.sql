INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Rymarchik Alexander');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Komok Fedor');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Zaharova Anna');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Chernetskaya Maria');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Gilevich Pavel');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Kuzmin Vlad');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Sachok Ilia');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Pantsialei Vitalii');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Gordienok Maksim');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Kulinkovich Victoria');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Zubovich Oleg');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Budko Alexei');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Savchanchik Anna');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Puchilo Ales');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Dotsenko Konstantin');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Mamai Victor');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Zagorskaya Tatiana');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Griblo Evgeniya');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Begun Sergei');
INSERT INTO NEWS.Author(author_id, author_name) VALUES(NEWS.author_seq.nextval, 'Muha Dmitriy');

INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'google');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'newfilm');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'javalab');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'gta5');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'apple');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'medicine');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'crime');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'universe');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'technology');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'bmw');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'yandex');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'dota2');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'gabe newell');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'epam');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'task');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'iphone');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'bill gates');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'steve jobs');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'carcrash');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'audiR8');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'dell');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'breaking news');
INSERT INTO NEWS.Tag(tag_id, tag_name) VALUES(NEWS.tag_seq.nextval, 'danger');

INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title1', 'short_text1', 'full_text1', TO_DATE('2014-12-12 17:22:00','YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-01-03','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title2', 'short_text2', 'full_text2', TO_DATE('2015-01-11 12:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title3', 'short_text3', 'full_text3', TO_DATE('2015-01-12 12:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title4', 'short_text4', 'full_text4', TO_DATE('2015-01-13 12:12:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-02-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title5', 'short_text5', 'full_text5', TO_DATE('2015-02-01 11:11:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title6', 'short_text6', 'full_text6', TO_DATE('2015-02-02 11:11:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title7', 'short_text7', 'full_text7', TO_DATE('2015-02-03 11:11:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-03-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title8', 'short_text8', 'full_text8',  TO_DATE('2015-03-11 16:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-15', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title9', 'short_text9', 'full_text9',  TO_DATE('2015-03-12 16:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-15','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title10', 'short_text10', 'full_text10', TO_DATE('2015-03-13 17:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-15','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title11', 'short_text11', 'full_text11', TO_DATE('2014-12-22 07:22:00','YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-01-03','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title12', 'short_text12', 'full_text12', TO_DATE('2015-01-21 12:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title13', 'short_text13', 'full_text13', TO_DATE('2015-01-22 12:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title14', 'short_text14', 'full_text14', TO_DATE('2015-01-23 12:12:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-02-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title15', 'short_text15', 'full_text15', TO_DATE('2015-02-21 11:11:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title16', 'short_text16', 'full_text16', TO_DATE('2015-02-22 11:11:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-11','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title17', 'short_text17', 'full_text17', TO_DATE('2015-02-23 11:11:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-03-11', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title18', 'short_text18', 'full_text18',  TO_DATE('2015-03-21 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-25', 'YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title19', 'short_text19', 'full_text19',  TO_DATE('2015-03-22 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-25','YYYY-MM-DD'));
INSERT INTO NEWS.News(news_id, title, short_text, full_text, creation_date, modification_date)
VALUES(NEWS.news_seq.nextval, 'title20', 'short_text20', 'full_text20', TO_DATE('2015-03-23 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-25','YYYY-MM-DD'));

INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'Good news!', TO_DATE('2015-04-04 22:22:00','YYYY-MM-DD HH24:MI:SS'), 1);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'sad=(', TO_DATE('2015-04-04 20:20:00','YYYY-MM-DD HH24:MI:SS'), 1);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'Its unbelievable!', TO_DATE('2015-04-04 17:30:00','YYYY-MM-DD HH24:MI:SS'), 12);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'i hope it will work..', TO_DATE('2015-04-04 21:30:00','YYYY-MM-DD HH24:MI:SS'), 13);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'OMG!', TO_DATE('2015-04-04 14:14:00','YYYY-MM-DD HH24:MI:SS'), 4);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'nice', TO_DATE('2015-04-04 15:15:00','YYYY-MM-DD HH24:MI:SS'), 15);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'awful news for me', TO_DATE('2015-04-04 13:13:00','YYYY-MM-DD HH24:MI:SS'), 6);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'interesting quote', TO_DATE('2015-04-04 19:19:00','YYYY-MM-DD HH24:MI:SS'), 7);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'LOL:D', TO_DATE('2015-04-04 17:17:00','YYYY-MM-DD HH24:MI:SS'), 8);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'hope hes fine', TO_DATE('2015-04-04 18:18:00','YYYY-MM-DD HH24:MI:SS'), 19);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'what a nice trick!', TO_DATE('2015-04-04 22:22:00','YYYY-MM-DD HH24:MI:SS'), 11);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'ITS NOT FUNNY!', TO_DATE('2015-04-04 20:20:00','YYYY-MM-DD HH24:MI:SS'), 12);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'hahaha', TO_DATE('2015-04-04 17:30:00','YYYY-MM-DD HH24:MI:SS'), 2);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'Everybody lies', TO_DATE('2015-04-04 21:30:00','YYYY-MM-DD HH24:MI:SS'), 3);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'i agree with him!', TO_DATE('2015-04-04 14:14:00','YYYY-MM-DD HH24:MI:SS'), 14);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'believe in justice', TO_DATE('2015-04-04 15:15:00','YYYY-MM-DD HH24:MI:SS'), 5);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'hm, sounds good', TO_DATE('2015-04-04 13:13:00','YYYY-MM-DD HH24:MI:SS'), 16);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'im shocked..', TO_DATE('2015-04-04 19:19:00','YYYY-MM-DD HH24:MI:SS'), 17);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'WTF?!', TO_DATE('2015-04-04 17:17:00','YYYY-MM-DD HH24:MI:SS'), 18);
INSERT INTO NEWS.Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(NEWS.comments_seq.nextval,'"stupid comment"', TO_DATE('2015-04-04 18:18:00','YYYY-MM-DD HH24:MI:SS'), 9);

INSERT INTO NEWS.news_author(news_id, author_id) VALUES(1,3);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(2,1);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(3,4);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(4,2);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(5,5);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(6,7);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(7,6);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(8,8);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(9,11);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(10,9);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(11,12);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(12,10);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(13,14);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(14,13);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(15,15);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(16,16);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(17,19);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(18,17);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(19,20);
INSERT INTO NEWS.news_author(news_id, author_id) VALUES(20,18);


INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(1,1);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(2,4);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(3,2);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(4,3);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(5,15);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(6,16);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(7,17);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(8,18);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(9,10);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(10,9);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(11,14);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(12,13);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(13,12);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(14,11);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(15,5);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(16,6);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(17,7);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(18,8);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(19,20);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(20,19);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(11,1);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(12,2);
INSERT INTO NEWS.news_tag(news_id, tag_id) VALUES(13,3);